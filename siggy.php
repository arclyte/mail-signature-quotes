<?php

/**
 * Updates the Apple Mail Signature Template with a random quote
 */

$signature_files = [
	"/Users/jamesalday/Library/Mail/V2/MailData/Signatures/ED957E0E-0146-43D8-9132-6B063D0117A1.mailsignature",
	"/Users/jamesalday/Library/Mobile Documents/com~apple~mail/Data/MailData/Signatures/ubiquitous_ED957E0E-0146-43D8-9132-6B063D0117A1.mailsignature"
];

$db = new SQLite3('/Users/jamesalday/Documents/mail_sigs/mailsigs.db');
$results = $db->query('SELECT q.quote, q.source, a.name FROM quotes q JOIN authors a ON q.author_id = a.id ORDER BY RANDOM() LIMIT 1');
$quote = $results->fetchArray(SQLITE3_ASSOC);

$quote_string = "\"{$quote['quote']}\"<br>- {$quote['name']}";

if (!empty($quote['source'])) {
	$quote_string .= " ({$quote['source']})";
}

foreach ($signature_files as $signature_file) {
	$signature = file_get_contents($signature_file);
	$new_signature = preg_replace('/\<!--QUOTE--\>=\n[^\<]*\s*(?:\<br>)?[^\<]*<!--ENDQUOTE-->/', "<!--QUOTE-->=\n" . $quote_string . '<!--ENDQUOTE-->', $signature);
	file_put_contents($signature_file, $new_signature);
}

exit;